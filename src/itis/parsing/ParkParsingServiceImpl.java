package itis.parsing;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.IOException;

import itis.parsing.annotations.FieldName;
import itis.parsing.annotations.MaxLength;
import itis.parsing.annotations.NotBlank;

import java.lang.reflect.InvocationTargetException;

public class ParkParsingServiceImpl implements ParkParsingService {

    //Парсит файл в обьект класса "Park", либо бросает исключение с информацией об ошибках обработки
    @Override
    public Park parseParkData(String parkDatafilePath) throws ParkParsingException {
        try {
            File file = new File(parkDatafilePath);
            FileReader fileReader = new FileReader(file);

            BufferedReader bufferedReader = new BufferedReader(fileReader);
            ArrayList<String> strings = new ArrayList<>();
            HashMap<String, String> map = new HashMap<>();

            while (true) {
                String parkString = bufferedReader.readLine();

                if (parkString == null) {
                    break;
                }

                strings.add(parkString);
                String[] stringParkArr = parkString.split("\":");
                if (stringParkArr.length == 2) {
                    String name1 = stringParkArr[0].replace("\"", "");
                    String value1 = name1.replace(" ", "");

                    name1 = stringParkArr[1].replace("\"", "");
                    String value2= name1.replace(" ", "");
                    map.put(value1, value2);
                }
            }
            bufferedReader.close();
            Class parkingClass = Park.class;

            Constructor<? extends Park> declaredConstructor = parkingClass.getDeclaredConstructor();
            declaredConstructor.setAccessible(true);
            Park park = declaredConstructor.newInstance();
            Field[] fields = parkingClass.getDeclaredFields();

            for (int i = 0; i < fields.length; i++) {
                String[] fieldName = fields[i].toString().split("\\.");
                String stringValueOfField = fieldName[fieldName.length - 1];

                Field declaredField = parkingClass.getDeclaredField(stringValueOfField);
                declaredField.setAccessible(true);


                if (stringValueOfField.equals("foundationYear")) {
                    String[] time = map.get("foundationYear").split("-");
                    declaredField.set(park, LocalDate.of(Integer.parseInt(time[0]), Integer.parseInt(time[1]), Integer.parseInt(time[2])));
                } else {
                    declaredField.set(park, map.get(stringValueOfField));
                }

                for (Annotation annotation : declaredField.getAnnotations()) {
                    if (annotation instanceof FieldName) {
                        declaredField.set(park, map.get("title"));
                    }
                    if (annotation instanceof MaxLength){
                        Field field = parkingClass.getDeclaredField(stringValueOfField);

                    }
                    if (annotation instanceof NotBlank){
                        Field field = parkingClass.getDeclaredField(stringValueOfField);
                    }
                }
            }
            return park;
        } catch (IOException | NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }

        return null;
    }
}